const JSDOC_HEAD = "/**\n"
const JSDOC_LINE = " * "
const JSDOC_END = " */"
const JSDOC_RETURN = "@returns"
const JSDOC_TYPE = "@type"
const JSDOC_DESCRIPTION = " - "
const JSDOC_PARAM = "@param"
const ONE_LINE = "//"


/**
 * 生成类型注释
 * @param { string } type - 类型
 * @returns { string }
 */
function createTypeStr(type) {
    return ` { ${type} } `
}


/**
 * 生成类型住宿行
 * @param { string } type 类型
 * @param { string } des 描述
 * @returns { string }
 */
function createTypeLine(type, des) {
    return `${JSDOC_LINE}${JSDOC_TYPE}${createTypeStr(type)}${JSDOC_DESCRIPTION}${des}\n`
}


/**
 * 生成参数注释行
 * @param { string } type - 类型
 * @param { string } name - 参数名
 * @param { string } des - 描述
 * @returns { string }
 */
function createParamLine(type, name, des) {
    return `${JSDOC_LINE}${JSDOC_PARAM}${createTypeStr(type)}${name}${JSDOC_DESCRIPTION}${des}\n`
}


/**
 * 生成返回值注释行
 * @param { string } type - 类型
 * @param { string } des - 描述
 * @returns { string }
 */
function createReturnLine(type, des) {
    return `${JSDOC_LINE}${JSDOC_RETURN}${createTypeStr(type)}${JSDOC_DESCRIPTION}${des}\n`
}


/**
 * 函数注释生成
 * @param { {type:string,des:string,name:string}[] } param 
 * @param { {type:string,des:string} } returns 
 * @param { string } [des=''] 
 * @returns { string }
 */
function createFunAnn(param = [], returns = {}, des = 'asdfasdf') {
    let content = `${JSDOC_HEAD}${JSDOC_LINE}${des}\n`
    param.forEach((d) => {
        content = content.concat(createParamLine(d.type, d.name, d.des))
    })
    content = content.concat(createReturnLine(returns.type, returns.des))
    return `${content}${JSDOC_END}`
}


/**
 * 属性注释生成
 * @param { {type:string,des:string}[] } type 
 * @returns { string }
 */
function createVarAnn(type = []) {
    let content = ''
    type.forEach((d) => {
        content = content.concat(createTypeLine(d.type, d.des))
    })
    return `${JSDOC_HEAD}${content}${JSDOC_END}`
}


export {
    JSDOC_HEAD,
    JSDOC_LINE,
    JSDOC_END,
    JSDOC_RETURN,
    JSDOC_TYPE,
    JSDOC_DESCRIPTION,
    JSDOC_PARAM,
    ONE_LINE,
    createParamLine,
    createReturnLine,
    createTypeLine,
    createTypeStr,
    createFunAnn,
    createVarAnn
}